﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.OleDb;
using Microsoft.Office.Interop.Excel;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form9 : Form
    {
        int i, j, k, l, h;
        public OleDbConnection odc = new OleDbConnection();

        public Form9(string comeon)
        {
            InitializeComponent();
            try
            {
                odc.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\Voting System.accdb;
Persist Security Info=False;";
                label6.Text = WindowsFormsApplication1.Properties.Settings.Default.Twofive.ToString();
                label7.Text = WindowsFormsApplication1.Properties.Settings.Default.Twosix.ToString();
                label8.Text = WindowsFormsApplication1.Properties.Settings.Default.Twoseven.ToString();
                label9.Text = WindowsFormsApplication1.Properties.Settings.Default.Twoeight.ToString();
                label13.Text = comeon;
            }
            catch (Exception ex)
            { }
            finally
            { }
            System.Windows.Forms.Application.DoEvents();
        }

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("G.html");
            try
            {
                var bm = new Bitmap(pictureBox1.Image);
                var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
                var g = Graphics.FromImage(bm);
                g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
                pictureBox1.Image = bm;
                DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    a++;
                    int ba = a + WindowsFormsApplication1.Properties.Settings.Default.Twofive;
                    label6.Text = ba.ToString();
                    WindowsFormsApplication1.Properties.Settings.Default.Twofive = Convert.ToInt32(label6.Text);
                    WindowsFormsApplication1.Properties.Settings.Default.Save();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                   
                    SpeechSynthesizer ss = new SpeechSynthesizer();
                    ss.Speak("Thanks for voting");
                    Thread.Sleep(1000);
                    //MessageBox.Show("Thanks for Voting!");
                    Form1 f1 = new Form1();
                    f1.Show();
                    this.Hide();
                    MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
                }
                else if (dia == DialogResult.No)
                {

                    pictureBox1.ImageLocation = @"C:\Users\Public\Documents\Voting Images\Y.jpg";
                }
                //MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
           
            
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
               // int j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
               
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Public Relation Officer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                //System.Windows.Forms.Application.Restart();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("G.html");
            try
            {
                var bm = new Bitmap(pictureBox2.Image);
                var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
                var ga = Graphics.FromImage(bm);
                ga.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
                pictureBox2.Image = bm;
                DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    b++;
                    int bb = b + WindowsFormsApplication1.Properties.Settings.Default.Twosix;
                    label7.Text = bb.ToString();
                    WindowsFormsApplication1.Properties.Settings.Default.Twosix = Convert.ToInt32(label7.Text);
                    WindowsFormsApplication1.Properties.Settings.Default.Save();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                   
                    SpeechSynthesizer ss = new SpeechSynthesizer();
                    ss.Speak("Thanks for voting");
                    Thread.Sleep(1000);
                     Form1 f1 = new Form1();
                     f1.Show();
                   // System.Windows.Forms.Application.Restart();
                    this.Hide();
                    MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
                }
                    
                else if (dia == DialogResult.No)
                {

                    pictureBox2.ImageLocation = @"C:\Users\Public\Documents\Voting Images\Z.jpg";
                }
                //MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
                //MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
            //}
            //catch (Exception ex)
            //{

            //}
            //finally
            //{
            //}
           
            //try
            //{
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
               //int j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
                
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Public Relation Officer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                //System.Windows.Forms.Application.Restart();
               // this.Hide();
               
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        { /*StreamWriter sw = new StreamWriter("G.html");
            try{
            var bm = new Bitmap(pictureBox3.Image);
            var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
            var g = Graphics.FromImage(bm);
            g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
            pictureBox3.Image = bm;
            DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
            if (dia == DialogResult.Yes)
            {
                c++;
                int wc = c + WindowsFormsApplication1.Properties.Settings.Default.Twoseven;
                label8.Text = wc.ToString();
                WindowsFormsApplication1.Properties.Settings.Default.Twoseven = Convert.ToInt32(label8.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;
                MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
                SpeechSynthesizer ss = new SpeechSynthesizer();
                ss.Speak("Thanks for voting");
                Thread.Sleep(1000);
                Form1 f1 = new Form1();
                f1.Show();
                this.Hide();
            }
            else if (dia == DialogResult.No)
            {

                pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\ZA.jpg";
            }
             l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text) + Convert.ToInt32(label8.Text) + Convert.ToInt32(label9.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                int j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
                
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Public Relation Officer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
               
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sw.Close();
            }
            */
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            /*var bm = new Bitmap(pictureBox4.Image);
            var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
            var g = Graphics.FromImage(bm);
            g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
            //g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(100, 100));
            pictureBox4.Image = bm;
            DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
            if (dia == DialogResult.Yes)
            {
                d++;
                int bd = d + WindowsFormsApplication1.Properties.Settings.Default.Twoeight;
                label9.Text = bd.ToString();
                WindowsFormsApplication1.Properties.Settings.Default.Twoeight = Convert.ToInt32(label9.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;
                SpeechSynthesizer ss = new SpeechSynthesizer();
                ss.Speak("Thanks for voting");
                Thread.Sleep(1000);
                Form1 f1 = new Form1();
                f1.Show();
                this.Hide();
            }
            else if (dia == DialogResult.No)
            {

                pictureBox4.ImageLocation = @"C:\Users\Public\Documents\Voting Images\ZB.jpg";
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SpeechSynthesizer ss = new SpeechSynthesizer();
                ss.Speak("Thanks for voting");
                Thread.Sleep(1000);
                DialogResult result = MessageBox.Show("Are you sure you want to go to the next page?", "Confirmation", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    
                    Form1 f1 = new Form1();
                   
                    f1.Show();
                    this.Hide();
                    if (label13.Text == "")
                    {
                        MessageBox.Show("Thanks for voting");

                    }
                    else
                    {
                        MessageBox.Show(label13.Text.Remove(0, 7) + ", thanks for voting!");
                    }
                    
                }
                    
                else if (result == DialogResult.No)
                { 
               
                
                }
                //Application.Restart();
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            StreamWriter sw = new StreamWriter("G.html");
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                //int j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
               
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Public Relation Officer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr><tr><th>{6}</td><th>{7}</td><th>{8}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
               
                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }

            System.Windows.Forms.Application.DoEvents();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            try
            {
                timer1.Start();
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                label11.Visible = false;
                label12.Visible = false;
                label5.Visible = false;
                label4.Visible = false;
                textBox1.Visible = false;
                textBox2.Visible = false;
                button4.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;
                textBox11.Visible = false;
                textBox12.Visible = false;
                pictureBox1.ImageLocation = @"C:\Users\Public\Documents\Voting Images\Y.jpg";
                pictureBox2.ImageLocation = @"C:\Users\Public\Documents\Voting Images\Z.jpg";
                //pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\ZA.jpg";
            }
            catch (Exception ex)
            { }
            finally
            { }
            //pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\ZA.jpg";
            //pictureBox4.ImageLocation = @"C:\Users\Public\Documents\Voting Images\ZB.jpg";
            try
            {
                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                string query = "Select Name from Title where ID=7 ";
                oc.CommandText = query;
                OleDbDataReader reader = oc.ExecuteReader();

                while (reader.Read())
                {
                    label1.Text = reader["Name"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
                OleDbCommand oc1 = new OleDbCommand();
                oc1.Connection = odc;
                string query1 = "Select BottomMessage from Title where ID=7 ";
                oc1.CommandText = query1;
                OleDbDataReader reader1 = oc1.ExecuteReader();

                while (reader1.Read())
                {
                    label10.Text = reader1["BottomMessage"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
               
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);


            }
            finally 
            {
                odc.Close();
            }
            try
            {
                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                string query = "Select Name from Pro where ID=1 ";
                oc.CommandText = query;
                OleDbDataReader reader = oc.ExecuteReader();

                while (reader.Read())
                {
                    label2.Text = reader["Name"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
                //MessageBox.Show("You are connected");
                OleDbCommand occ = new OleDbCommand();
                occ.Connection = odc;
                string kuery = "Select Name from Pro where ID=2 ";
                occ.CommandText = kuery;
                OleDbDataReader read = occ.ExecuteReader();
                while (read.Read())
                {
                    label3.Text = read["Name"].ToString();

                }
                /* OleDbCommand oca = new OleDbCommand();
                 oca.Connection = odc;
                 string uery = "Select Name from Pro where ID=3 ";
                 oca.CommandText = uery;
                 OleDbDataReader readu = oca.ExecuteReader();
                 while (readu.Read())
                 {
                     label4.Text = readu["Name"].ToString();

                 }
                
                 OleDbCommand ocb = new OleDbCommand();
                 ocb.Connection = odc;
                 string kuer = "Select Name from Pro where ID=4 ";
                 ocb.CommandText = kuer;
                 OleDbDataReader readb = ocb.ExecuteReader();
                 while (readb.Read())
                 {
                     label5.Text = readb["Name"].ToString();

                 }*/
               
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally 
            {
                odc.Close();
            
            
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                int re1, re2, re3, re4;
                re1 = 0;
                re2 = 0;
                re3 = 0;
                re4 = 0;
                label6.Text = re1.ToString();
                label7.Text = re2.ToString();
                //label8.Text = re3.ToString();
               // label9.Text = re4.ToString();
                WindowsFormsApplication1.Properties.Settings.Default.Twofive = Convert.ToInt32(label6.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                WindowsFormsApplication1.Properties.Settings.Default.Twosix = Convert.ToInt32(label7.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
               /*WindowsFormsApplication1.Properties.Settings.Default.Twoseven = Convert.ToInt32(label8.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                WindowsFormsApplication1.Properties.Settings.Default.Twoeight = Convert.ToInt32(label9.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();*/
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button3_Click(object sender, EventArgs e)
        {

           
            
                /*Microsoft.Office.Interop.Excel._Application exc = new Microsoft.Office.Interop.Excel.Application();
                Workbook wb = exc.Workbooks.Add(XlSheetType.xlWorksheet);
                Worksheet ws = exc.ActiveSheet;
                exc.Visible = true;*/
                //ws.Cells[1, 1] = "Js1";
            
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                //int j = Convert.ToInt32(label8.Text) * 100 / l;
                // int h = Convert.ToInt32(label9.Text) * 100 / l;
                /*ws.Cells[1, 1] = "Name";
                ws.Cells[1, 2] = "Votes";
                ws.Cells[1, 3] = "Percentage";
                //ws.Cells[1, 4] = "Number of votes";
                //ws.Cells[1, 5] = "Gender";

                ws.Cells[2, 1] = label2.Text;
                ws.Cells[2, 2] = label6.Text;
                ws.Cells[2, 3] = i.ToString() + "%";

                ws.Cells[3, 1] = label3.Text;
                ws.Cells[3, 2] = label7.Text;
                ws.Cells[3, 3] = k.ToString() + "%";

                ws.Cells[4, 1] = label4.Text;
                ws.Cells[4, 2] = label8.Text;
                ws.Cells[4, 3] = j.ToString() + "%";

                ws.Cells[5, 1] = label5.Text;
                ws.Cells[5, 2] = label9.Text;
                ws.Cells[5, 3] = h.ToString() + "%";*/
                StreamWriter sw = new StreamWriter("G.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Public Relation Officer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Flush();
                sw.Dispose();
                sw.Close();
                System.Diagnostics.Process.Start("G.html");
                StreamReader sra = new StreamReader("A.html");
                textBox3.Text = sra.ReadToEnd();
                sra.Close();
                StreamReader srb = new StreamReader("B.html");
                textBox4.Text = srb.ReadToEnd();
                srb.Close();
                StreamReader src = new StreamReader("C.html");
                 textBox5.Text = src.ReadToEnd();
                src.Close();
                StreamReader srd = new StreamReader("D.html");
                textBox6.Text = srd.ReadToEnd();
                srd.Close();
                StreamReader sre = new StreamReader("E.html");
                textBox7.Text = sre.ReadToEnd();
                sre.Close();
                StreamReader srf = new StreamReader("F.html");
                textBox8.Text = srf.ReadToEnd();
                srf.Close();
                StreamReader srg = new StreamReader("G.html");
                textBox9.Text = srg.ReadToEnd();
                srg.Close();
                /*  StreamReader srh = new StreamReader("H.html");
                 textBox10.Text = srh.ReadToEnd();
                 srh.Close();
                StreamReader sri = new StreamReader("I.html");
                 textBox11.Text = sri.ReadToEnd();
                 sri.Close();
                 StreamReader srj = new StreamReader("J.html");
                 textBox12.Text = srj.ReadToEnd();
                 srj.Close();*/
                StreamWriter swa = new StreamWriter("All.html");
                swa.WriteLine(textBox3.Text);
                swa.WriteLine(textBox4.Text);
                swa.WriteLine(textBox5.Text);
                swa.WriteLine(textBox6.Text);
                swa.WriteLine(textBox7.Text);
                swa.WriteLine(textBox8.Text);
                swa.WriteLine(textBox9.Text);
                //swa.WriteLine(textBox10.Text);
                //swa.WriteLine(textBox11.Text);
                //swa.WriteLine(textBox12.Text);
                swa.Flush();
                swa.Dispose();
                swa.Close();
                System.Diagnostics.Process.Start("All.html");
            }
            catch (Exception ex)
            { }
            finally
            {  }
            System.Windows.Forms.Application.DoEvents();
        }

        private void Form9_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                DialogResult di = MessageBox.Show("Are you sure you want to stop voting?", "Confirmation", MessageBoxButtons.YesNo);
                if (di == DialogResult.Yes)
                {
                    return;
                    System.Windows.Forms.Application.Exit(); }
                else if (di == DialogResult.No)

                { e.Cancel = true; }
                
            }
            catch (Exception ex)
            { }
            finally
            {


            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void Form9_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                label11.Visible = true;
                label12.Visible = true;
                textBox1.Visible = true;
                textBox2.Visible = true;
                button4.Visible = true;
            }
            catch (Exception ex)
            {
            }
            finally 
            {
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                //int j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;

                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                oc.CommandText = "INSERT INTO [Result for PRO] ([Names],[Votes],[Percentages])  VALUES('" + label2.Text + "','" + label6.Text + "','" + i.ToString() + "')";
                oc.ExecuteNonQuery();


                OleDbCommand occ = new OleDbCommand();
                occ.Connection = odc;
                occ.CommandText = "INSERT INTO [Result for PRO] ([Names],[Votes],[Percentages])  VALUES('" + label3.Text + "','" + label7.Text + "','" + k.ToString() + "')";
                occ.ExecuteNonQuery();


                /* OleDbCommand ocd = new OleDbCommand();
               ocd.Connection = odc;
               ocd.CommandText = "INSERT INTO [Result for PRO] ([Names],[Votes],[Percentages])  VALUES('" + label4.Text + "','" + label8.Text + "','" + j.ToString() + "')";
               ocd.ExecuteNonQuery();


          OleDbCommand oce = new OleDbCommand();
            oce.Connection = odc;
            oce.CommandText = "INSERT INTO [Result for PRO] ([Names],[Votes],[Percentages])  VALUES('" + label5.Text + "','" + label9.Text + "','" + h.ToString() + "')";
            oce.ExecuteNonQuery();*/




            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
            }
            finally
            {
                odc.Close();
            }
            try
            {
                if (textBox1.Text == "awosusi" && textBox2.Text == "oluwatopebest")
                {
                    button1.Visible = true;
                    button2.Visible = true;
                    button3.Visible = true;
                    button5.Visible = true;
                    button6.Visible = true;
                    label6.Visible = true;
                    label7.Visible = true;
                    // label8.Visible = true;
                    //label9.Visible = true;
                }
                else
                {
                    MessageBox.Show("Access denied!");
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            System.Windows.Forms.Application.DoEvents();
        }
        
        private void textBox1_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(textBox1, "Please insert your Username");
        }

        private void textBox2_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(textBox2, "Please insert your Password");
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button4, "Please click to check result on windows form");
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button3, "Please click to check result on Excel page");
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button1, "Please click to go to the next page");
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button2, "Click to reset the results.");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox1, " Click to vote for this candidate");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox2, " Click to vote for this candidate");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            //toolTip1.SetToolTip(pictureBox3, " Click to vote for this candidate");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
           // toolTip1.SetToolTip(pictureBox4, " Click to vote for this candidate");
        }
        bool ani = true;
        private void timer1_Tick(object sender, EventArgs e)
        {

            try
            {
                label14.Text = DateTime.Now.ToLongTimeString();
                if (ani)
                    label13.Location = new System.Drawing.Point(label13.Location.X + 5, label13.Location.Y);
                else
                    label13.Location = new System.Drawing.Point(label13.Location.X - 1044, label13.Location.Y);
                if (label13.Location.X + label13.Width >= this.Width + 180)
                    ani = false;
                if (label13.Location.X <= 0)
                    ani = true;
            }
            catch (Exception ex)
            { }
            finally
            { }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Collation co = new Collation();
                co.Show();
                this.Hide();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void button6_Click(object sender, EventArgs e)
        {try{
            Form1 f1 = new Form1();
            f1.Show();
            this.Hide();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
