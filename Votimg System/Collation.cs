﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Collation : Form
    {
        OleDbConnection ole = new OleDbConnection();
        public Collation()
        {
            InitializeComponent();
            ole.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\Voting System.accdb;
Persist Security Info=False;";
        }

        private void Collation_Load(object sender, EventArgs e)
        {
            try
            {
            ole.Open();
            
                using (OleDbCommand olc = new OleDbCommand())
                {

                    
                    olc.CommandText = "SELECT*FROM [Final Result for Presidency]";
                    olc.Connection = ole;
                    DataTable dt = new DataTable();
                    OleDbDataAdapter oda = new OleDbDataAdapter(olc);
                    oda.Fill(dt);
                    dataGridView1.DataSource = dt;

                    olc.CommandText = "SELECT*FROM [Final Result for Secretary]";
                    olc.Connection = ole;
                    DataTable dt1 = new DataTable();
                    OleDbDataAdapter oda1 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt1);
                    dataGridView2.DataSource = dt1;

                    olc.CommandText = "SELECT*FROM [Final Result for Social Director]";
                    olc.Connection = ole;
                    DataTable dt2 = new DataTable();
                    OleDbDataAdapter oda2 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt2);
                    dataGridView3.DataSource = dt2;
                    olc.CommandText = "SELECT*FROM [Final Result for Financial Secretary]";
                    olc.Connection = ole;
                    DataTable dt3 = new DataTable();
                    OleDbDataAdapter oda3 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt3);
                    dataGridView4.DataSource = dt3;
                    olc.CommandText = "SELECT*FROM [Final Result for ASSecretary]";
                    olc.Connection = ole;
                    DataTable dt4 = new DataTable();
                    OleDbDataAdapter oda4 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt4);
                    dataGridView5.DataSource = dt4;
                    olc.CommandText = "SELECT*FROM [Final Result for Sport]";
                    olc.Connection = ole;
                    DataTable dt5 = new DataTable();
                    OleDbDataAdapter oda5 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt5);
                    dataGridView6.DataSource = dt5;
                    olc.CommandText = "SELECT*FROM [Final Result for PRO]";
                    olc.Connection = ole;
                    DataTable dt6 = new DataTable();
                    OleDbDataAdapter oda6 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt6);
                    dataGridView7.DataSource = dt6;
                    olc.CommandText = "SELECT*FROM [Final Result for VP]";
                    olc.Connection = ole;
                    DataTable dt7 = new DataTable();
                    OleDbDataAdapter oda7 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt7);
                    dataGridView8.DataSource = dt7;
                   
                    olc.CommandText = "SELECT*FROM [Final Result for Treasurer]";
                    olc.Connection = ole;
                    DataTable dt8 = new DataTable();
                    OleDbDataAdapter oda8 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt8);
                    dataGridView9.DataSource = dt8;
                    olc.CommandText = "SELECT*FROM [Final Result for Librarian]";
                    olc.Connection = ole;
                    DataTable dt9 = new DataTable();
                    OleDbDataAdapter oda9 = new OleDbDataAdapter(olc);
                    oda1.Fill(dt9);
                    dataGridView10.DataSource = dt9;

                }
                
            } 
            catch (Exception ex)
            {



            }
            finally
            {
                ole.Close();
            
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewCell cell = null;
                foreach (DataGridViewCell dcell in dataGridView1.SelectedCells)
                {

                    cell = dcell;
                    break;


                }
                if (cell != null)
                {
                    DataGridViewRow row = cell.OwningRow;

                    StreamWriter sw = new StreamWriter("FinalA.html");
                    sw.WriteLine("<html>");
                    sw.WriteLine("<head>");
                    sw.WriteLine("<title>{0}</title>", label1.Text);
                    sw.WriteLine("</head>");
                    sw.WriteLine("<body bgcolor='grey'>");
                    sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>President Election Result</h1>");
                    sw.WriteLine("<hr>");
                    sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView1.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[7].Value + "</td><th>" + dataGridView1.Rows[0].Cells[8].Value + "%</td><th>" + dataGridView1.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView1.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView1.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView1.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView1.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView1.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView1.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView1.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                    sw.WriteLine("</body>");
                    sw.WriteLine("</html>");
                    sw.Close();
                    System.Diagnostics.Process.Start("FinalA.html");
                }
            }
            catch (Exception ex)
            {

            }
            finally 
            {
            
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter sw = new StreamWriter("FinalB.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Secretary Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView2.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView2.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView2.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView2.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView2.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView2.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView2.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView2.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalB.html");
            }
            catch (Exception ex)
            { }
            finally 
            
            { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter sw = new StreamWriter("FinalC.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Social Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView1.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView3.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView3.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView3.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView3.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView3.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView3.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView3.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalC.html");
            }
            catch (Exception ex)
            {
            
            }
            finally 
            { 
            
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter sw = new StreamWriter("FinalD.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px> Financial Secretary Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView4.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView4.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView4.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView4.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView4.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView4.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView4.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView4.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalD.html");
            }
            catch (Exception ex)
            { }
            finally 
            { 
            
            
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                StreamWriter sw = new StreamWriter("FinalE.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Assistant Secretary General Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView5.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView5.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView5.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView5.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView5.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView5.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView5.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalE.html");
            }
            catch (Exception ex)
            { }
            finally 
            {
            
            
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter sw = new StreamWriter("FinalF.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Sport Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView6.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView6.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView6.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[7].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[8].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView6.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView6.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView6.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView6.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView6.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView5.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalF.html");
            }
            catch (Exception ex)
            { 
            
            }
            finally
            {
            
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {

                StreamWriter sw = new StreamWriter("FinalG.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>PRO Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView7.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView7.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView7.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[7].Value + "</td><th>" + dataGridView7.Rows[1].Cells[8].Value + "</td><th>" + dataGridView7.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView7.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView7.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView7.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView7.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView7.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalG.html");
            }
            catch (Exception ex)
            { }
            finally 
            {
            
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {


            }
            catch (Exception)
            {

            }
        }

        private void dataGridView5_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[0].Cells[9].Value = "";
            dataGridView1.Rows[1].Cells[9].Value = "";
            dataGridView1.Rows[2].Cells[9].Value = "";
            dataGridView1.Rows[3].Cells[9].Value = "";
            dataGridView1.Rows[0].Cells[8].Value = "";
            dataGridView1.Rows[1].Cells[8].Value = "";
            dataGridView1.Rows[2].Cells[8].Value = "";
            dataGridView1.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView1.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView1.Rows[0].Cells[2].Value);
                }
                if (dataGridView1.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView1.Rows[0].Cells[3].Value);
                }
                if (dataGridView1.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView1.Rows[0].Cells[4].Value);
                }
                if (dataGridView1.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView1.Rows[0].Cells[5].Value);
                }
                if (dataGridView1.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView1.Rows[0].Cells[6].Value);
                }
                //f = Convert.ToInt32(dataGridView1.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView1.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView1.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView1.Rows[1].Cells[2].Value);
                }
                if (dataGridView1.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView1.Rows[1].Cells[3].Value);
                }
                if (dataGridView1.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView1.Rows[1].Cells[4].Value);
                }
                if (dataGridView1.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView1.Rows[1].Cells[5].Value);
                }
                if (dataGridView1.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView1.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView1.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView1.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView1.Rows[2].Cells[2].Value);
                }
                if (dataGridView1.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView1.Rows[2].Cells[3].Value);
                }
                if (dataGridView1.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView1.Rows[2].Cells[4].Value);
                }
                if (dataGridView1.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView1.Rows[2].Cells[5].Value);
                }
                if (dataGridView1.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView1.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView1.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView1.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView1.Rows[3].Cells[2].Value);
                }
                if (dataGridView1.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView1.Rows[3].Cells[3].Value);
                }
                if (dataGridView1.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView1.Rows[3].Cells[4].Value);
                }
                if (dataGridView1.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView1.Rows[3].Cells[5].Value);
                }
                if (dataGridView1.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView1.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView1.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView1.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView1.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView1.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView1.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView1.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView1.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView1.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView1.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally 
            { 
            
            
            }
            try
            {
                if (dataGridView1.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView1.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView1.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView1.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView1.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView1.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView1.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView1.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView1.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView1.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView1.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView1.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView1.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView1.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView1.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView1.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally 
            { 
            
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           /* try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView1.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView1.Rows[0].Cells[3].Value);
                }
                if (dataGridView1.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView1.Rows[0].Cells[3].Value);
                }
                if (dataGridView1.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView1.Rows[0].Cells[4].Value);
                }
                if (dataGridView1.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView1.Rows[0].Cells[5].Value);
                }
                if (dataGridView1.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView1.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView1.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView1.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView1.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView1.Rows[1].Cells[2].Value);
                }
                if (dataGridView1.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView1.Rows[1].Cells[3].Value);
                }
                if (dataGridView1.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView1.Rows[1].Cells[4].Value);
                }
                if (dataGridView1.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView1.Rows[1].Cells[5].Value);
                }
                if (dataGridView1.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView1.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView1.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView1.Rows[1].Cells[7].Value = m;
                
                //string q = o.ToString();
                //string r = p.ToString();
              int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
              if (dataGridView1.Rows[2].Cells[2].Value.ToString() == "")
              { dataGridView1.Rows[2].Cells[2].Value = 0; }
              else
              {
                  s = Convert.ToInt32(dataGridView1.Rows[2].Cells[2].Value);
              }
              if (dataGridView1.Rows[2].Cells[3].Value.ToString() == "")
              { dataGridView1.Rows[2].Cells[3].Value = 0; }
              else
              {
                  t = Convert.ToInt32(dataGridView1.Rows[2].Cells[3].Value);
              }
              if (dataGridView1.Rows[2].Cells[4].Value.ToString() == "")
              { dataGridView1.Rows[2].Cells[4].Value = 0; }
              else
              {
                  u = Convert.ToInt32(dataGridView1.Rows[2].Cells[4].Value);
              }
              if (dataGridView1.Rows[2].Cells[5].Value.ToString() == "")
              { dataGridView1.Rows[2].Cells[5].Value = 0; }
              else
              {
                  v = Convert.ToInt32(dataGridView1.Rows[2].Cells[5].Value);
              }
              if (dataGridView1.Rows[2].Cells[6].Value.ToString() == "")
              { dataGridView1.Rows[2].Cells[6].Value = 0; }
              else
              {
                  w = Convert.ToInt32(dataGridView1.Rows[2].Cells[6].Value);
              }
              x = s + t + u + v + w;
              dataGridView1.Rows[2].Cells[7].Value = x;
              int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
              if (dataGridView1.Rows[3].Cells[2].Value.ToString() == "")
              { dataGridView1.Rows[3].Cells[2].Value = 0; }
              else
              {
                  za = Convert.ToInt32(dataGridView1.Rows[3].Cells[2].Value);
              }
              if (dataGridView1.Rows[3].Cells[3].Value.ToString() == "")
              { dataGridView1.Rows[3].Cells[3].Value = 0; }
              else
              {
                  zb = Convert.ToInt32(dataGridView1.Rows[3].Cells[3].Value);
              }
              if (dataGridView1.Rows[3].Cells[4].Value.ToString() == "")
              { dataGridView1.Rows[3].Cells[4].Value = 0; }
              else
              {
                  zc = Convert.ToInt32(dataGridView1.Rows[3].Cells[4].Value);
              }
              if (dataGridView1.Rows[3].Cells[5].Value.ToString() == "")
              { dataGridView1.Rows[3].Cells[5].Value = 0; }
              else
              {
                  zd = Convert.ToInt32(dataGridView1.Rows[3].Cells[5].Value);
              }
              if (dataGridView1.Rows[3].Cells[6].Value.ToString() == "")
              { dataGridView1.Rows[3].Cells[6].Value = 0; }
              else
              {
                  ze = Convert.ToInt32(dataGridView1.Rows[3].Cells[6].Value);
              }
              z = za + zb + zc + zd + ze;
              dataGridView1.Rows[3].Cells[7].Value = z;
              int n, o, p,y,zf;

              if (dataGridView1.Rows[0].Cells[7].Value != null)
              //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
              //else
              {
                  n = g + m+x+z;
                  //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                  o = (g * 100) / n;
                  dataGridView1.Rows[0].Cells[8].Value = o.ToString();
              }

              if (dataGridView1.Rows[1].Cells[7].Value.ToString() != null)
              //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
              //else
              {
                  n = g + m+x+z;
                  //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                  p = (m * 100) / n;
                  dataGridView1.Rows[1].Cells[8].Value = p.ToString();
              }
              if (dataGridView1.Rows[2].Cells[7].Value.ToString() != null)
              //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
              //else
              {
                  n = g + m + x+z;
                  //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                  y = (x * 100) / n;
                  dataGridView1.Rows[2].Cells[8].Value = y.ToString();
              }
              if (dataGridView1.Rows[3].Cells[7].Value.ToString() != null)
              //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
              //else
              {
                  n = g + m + x + z;
                  //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                  zf = (z * 100) / n;
                  dataGridView1.Rows[3].Cells[8].Value = zf.ToString();
              }
            }
            catch (Exception ex)
            { }
            */
        }

        private void button9_Click(object sender, EventArgs e)
        {
            dataGridView2.Rows[0].Cells[9].Value = "";
            dataGridView2.Rows[1].Cells[9].Value = "";
            dataGridView2.Rows[2].Cells[9].Value = "";
            dataGridView2.Rows[3].Cells[9].Value = "";
            dataGridView2.Rows[0].Cells[8].Value = "";
            dataGridView2.Rows[1].Cells[8].Value = "";
            dataGridView2.Rows[2].Cells[8].Value = "";
            dataGridView2.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView2.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView2.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView2.Rows[0].Cells[2].Value);
                }
                if (dataGridView2.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView2.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView2.Rows[0].Cells[3].Value);
                }
                if (dataGridView2.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView2.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView2.Rows[0].Cells[4].Value);
                }
                if (dataGridView2.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView2.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView2.Rows[0].Cells[5].Value);
                }
                if (dataGridView2.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView2.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView2.Rows[0].Cells[6].Value);
                }
                //f = Convert.ToInt32(dataGridView2.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView2.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView2.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView2.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView2.Rows[1].Cells[2].Value);
                }
                if (dataGridView2.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView2.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView2.Rows[1].Cells[3].Value);
                }
                if (dataGridView2.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView2.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView2.Rows[1].Cells[4].Value);
                }
                if (dataGridView2.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView2.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView2.Rows[1].Cells[5].Value);
                }
                if (dataGridView2.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView2.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView2.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView2.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView2.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView2.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView2.Rows[2].Cells[2].Value);
                }
                if (dataGridView2.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView2.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView2.Rows[2].Cells[3].Value);
                }
                if (dataGridView2.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView2.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView2.Rows[2].Cells[4].Value);
                }
                if (dataGridView2.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView2.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView2.Rows[2].Cells[5].Value);
                }
                if (dataGridView2.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView2.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView2.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView2.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView2.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView2.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView2.Rows[3].Cells[2].Value);
                }
                if (dataGridView2.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView2.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView2.Rows[3].Cells[3].Value);
                }
                if (dataGridView2.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView2.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView2.Rows[3].Cells[4].Value);
                }
                if (dataGridView2.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView2.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView2.Rows[3].Cells[5].Value);
                }
                if (dataGridView2.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView2.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView2.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView2.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView2.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView2.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView2.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView2.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView2.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView2.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView2.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView2.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            
            }
            try
            {
                if (dataGridView2.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView2.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView2.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView2.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView2.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView2.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView2.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView2.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView2.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView2.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView2.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView2.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView2.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView2.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView2.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView2.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            { 
            
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            dataGridView3.Rows[0].Cells[9].Value = "";
            dataGridView3.Rows[1].Cells[9].Value = "";
            dataGridView3.Rows[2].Cells[9].Value = "";
            dataGridView3.Rows[3].Cells[9].Value = "";
            dataGridView3.Rows[0].Cells[8].Value = "";
            dataGridView3.Rows[1].Cells[8].Value = "";
            dataGridView3.Rows[2].Cells[8].Value = "";
            dataGridView3.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView3.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView3.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView3.Rows[0].Cells[2].Value);
                }
                if (dataGridView3.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView3.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView3.Rows[0].Cells[3].Value);
                }
                if (dataGridView3.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView3.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView3.Rows[0].Cells[4].Value);
                }
                if (dataGridView3.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView3.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView3.Rows[0].Cells[5].Value);
                }
                if (dataGridView3.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView3.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView3.Rows[0].Cells[6].Value);
                }
                //f = Convert.ToInt32(dataGridView3.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView3.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView3.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView3.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView3.Rows[1].Cells[2].Value);
                }
                if (dataGridView3.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView3.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView3.Rows[1].Cells[3].Value);
                }
                if (dataGridView3.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView3.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView3.Rows[1].Cells[4].Value);
                }
                if (dataGridView3.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView3.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView3.Rows[1].Cells[5].Value);
                }
                if (dataGridView3.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView3.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView3.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView3.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView3.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView3.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView3.Rows[2].Cells[2].Value);
                }
                if (dataGridView3.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView3.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView3.Rows[2].Cells[3].Value);
                }
                if (dataGridView3.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView3.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView3.Rows[2].Cells[4].Value);
                }
                if (dataGridView3.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView3.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView3.Rows[2].Cells[5].Value);
                }
                if (dataGridView3.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView3.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView3.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView3.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView3.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView3.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView3.Rows[3].Cells[2].Value);
                }
                if (dataGridView3.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView3.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView3.Rows[3].Cells[3].Value);
                }
                if (dataGridView3.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView3.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView3.Rows[3].Cells[4].Value);
                }
                if (dataGridView3.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView3.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView3.Rows[3].Cells[5].Value);
                }
                if (dataGridView3.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView3.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView3.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView3.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView3.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView3.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView3.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView3.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView3.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView3.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView3.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView3.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally 
            { 
            
            }
            try
            {
                if (dataGridView3.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView3.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView3.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView3.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView3.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView3.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView3.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView3.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView3.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView3.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView3.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView3.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView3.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView3.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView3.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView3.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            
            { 
            
            
            
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            dataGridView4.Rows[0].Cells[9].Value = "";
            dataGridView4.Rows[1].Cells[9].Value = "";
            dataGridView4.Rows[2].Cells[9].Value = "";
            dataGridView4.Rows[3].Cells[9].Value = "";
            dataGridView4.Rows[0].Cells[8].Value = "";
            dataGridView4.Rows[1].Cells[8].Value = "";
            dataGridView4.Rows[2].Cells[8].Value = "";
            dataGridView4.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView4.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView4.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView4.Rows[0].Cells[2].Value);
                }
                if (dataGridView4.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView4.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView4.Rows[0].Cells[3].Value);
                }
                if (dataGridView4.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView4.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView4.Rows[0].Cells[4].Value);
                }
                if (dataGridView4.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView4.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView4.Rows[0].Cells[5].Value);
                }
                if (dataGridView4.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView4.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView4.Rows[0].Cells[6].Value);
                }
                //f = Convert.ToInt32(dataGridView4.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView4.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView4.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView4.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView4.Rows[1].Cells[2].Value);
                }
                if (dataGridView4.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView4.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView4.Rows[1].Cells[3].Value);
                }
                if (dataGridView4.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView4.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView4.Rows[1].Cells[4].Value);
                }
                if (dataGridView4.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView4.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView4.Rows[1].Cells[5].Value);
                }
                if (dataGridView4.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView4.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView4.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView4.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView4.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView4.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView4.Rows[2].Cells[2].Value);
                }
                if (dataGridView4.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView4.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView4.Rows[2].Cells[3].Value);
                }
                if (dataGridView4.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView4.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView4.Rows[2].Cells[4].Value);
                }
                if (dataGridView4.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView4.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView4.Rows[2].Cells[5].Value);
                }
                if (dataGridView4.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView4.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView4.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView4.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView4.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView4.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView4.Rows[3].Cells[2].Value);
                }
                if (dataGridView4.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView4.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView4.Rows[3].Cells[3].Value);
                }
                if (dataGridView4.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView4.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView4.Rows[3].Cells[4].Value);
                }
                if (dataGridView4.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView4.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView4.Rows[3].Cells[5].Value);
                }
                if (dataGridView4.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView4.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView4.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView4.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView4.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView4.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView4.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView4.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView4.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView4.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView4.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView4.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView4.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView4.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView4.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView4.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView4.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView4.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView4.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView4.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView4.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView4.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView4.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView4.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView4.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView4.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView4.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView4.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            dataGridView5.Rows[0].Cells[9].Value = "";
            dataGridView5.Rows[1].Cells[9].Value = "";
            dataGridView5.Rows[2].Cells[9].Value = "";
            dataGridView5.Rows[3].Cells[9].Value = "";
            dataGridView5.Rows[0].Cells[8].Value = "";
            dataGridView5.Rows[1].Cells[8].Value = "";
            dataGridView5.Rows[2].Cells[8].Value = "";
            dataGridView5.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView5.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView5.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView5.Rows[0].Cells[2].Value);
                }
                if (dataGridView5.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView5.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView5.Rows[0].Cells[3].Value);
                }
                if (dataGridView5.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView5.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView5.Rows[0].Cells[4].Value);
                }
                if (dataGridView5.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView5.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView5.Rows[0].Cells[5].Value);
                }
                if (dataGridView5.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView5.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView5.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView5.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView5.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView5.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView5.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView5.Rows[1].Cells[2].Value);
                }
                if (dataGridView5.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView5.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView5.Rows[1].Cells[3].Value);
                }
                if (dataGridView5.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView5.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView5.Rows[1].Cells[4].Value);
                }
                if (dataGridView5.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView5.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView5.Rows[1].Cells[5].Value);
                }
                if (dataGridView5.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView5.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView5.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView5.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView5.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView5.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView5.Rows[2].Cells[2].Value);
                }
                if (dataGridView5.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView5.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView5.Rows[2].Cells[3].Value);
                }
                if (dataGridView5.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView5.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView5.Rows[2].Cells[4].Value);
                }
                if (dataGridView5.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView5.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView5.Rows[2].Cells[5].Value);
                }
                if (dataGridView5.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView5.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView5.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView5.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView5.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView5.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView5.Rows[3].Cells[2].Value);
                }
                if (dataGridView5.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView5.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView5.Rows[3].Cells[3].Value);
                }
                if (dataGridView5.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView5.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView5.Rows[3].Cells[4].Value);
                }
                if (dataGridView5.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView5.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView5.Rows[3].Cells[5].Value);
                }
                if (dataGridView5.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView5.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView5.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView5.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView5.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView5.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView5.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView5.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView5.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView5.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView5.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView5.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView5.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView5.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView5.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView5.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView5.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView5.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView5.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView5.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView5.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView5.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView5.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView5.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView5.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView5.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView5.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView5.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            dataGridView6.Rows[0].Cells[9].Value = "";
            dataGridView6.Rows[1].Cells[9].Value = "";
            dataGridView6.Rows[2].Cells[9].Value = "";
            dataGridView6.Rows[3].Cells[9].Value = "";
            dataGridView6.Rows[0].Cells[8].Value = "";
            dataGridView6.Rows[1].Cells[8].Value = "";
            dataGridView6.Rows[2].Cells[8].Value = "";
            dataGridView6.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView6.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView6.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView6.Rows[0].Cells[2].Value);
                }
                if (dataGridView6.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView6.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView6.Rows[0].Cells[3].Value);
                }
                if (dataGridView6.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView6.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView6.Rows[0].Cells[4].Value);
                }
                if (dataGridView6.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView6.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView6.Rows[0].Cells[5].Value);
                }
                if (dataGridView6.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView6.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView6.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView6.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView6.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView6.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView6.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView6.Rows[1].Cells[2].Value);
                }
                if (dataGridView6.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView6.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView6.Rows[1].Cells[3].Value);
                }
                if (dataGridView6.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView6.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView6.Rows[1].Cells[4].Value);
                }
                if (dataGridView6.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView6.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView6.Rows[1].Cells[5].Value);
                }
                if (dataGridView6.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView6.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView6.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView6.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView6.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView6.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView6.Rows[2].Cells[2].Value);
                }
                if (dataGridView6.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView6.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView6.Rows[2].Cells[3].Value);
                }
                if (dataGridView6.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView6.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView6.Rows[2].Cells[4].Value);
                }
                if (dataGridView6.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView6.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView6.Rows[2].Cells[5].Value);
                }
                if (dataGridView6.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView6.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView6.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView6.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView6.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView6.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView6.Rows[3].Cells[2].Value);
                }
                if (dataGridView6.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView6.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView6.Rows[3].Cells[3].Value);
                }
                if (dataGridView6.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView6.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView6.Rows[3].Cells[4].Value);
                }
                if (dataGridView6.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView6.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView6.Rows[3].Cells[5].Value);
                }
                if (dataGridView6.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView6.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView6.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView6.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView6.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView6.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView6.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView6.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView6.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView6.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView6.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView6.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView6.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView6.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView6.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView6.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView6.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView6.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView6.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView6.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView6.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView6.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView6.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView6.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView6.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView6.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView6.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView6.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            dataGridView7.Rows[0].Cells[9].Value = "";
            dataGridView7.Rows[1].Cells[9].Value = "";
            dataGridView7.Rows[2].Cells[9].Value = "";
            dataGridView7.Rows[3].Cells[9].Value = "";
            dataGridView7.Rows[0].Cells[8].Value = "";
            dataGridView7.Rows[1].Cells[8].Value = "";
            dataGridView7.Rows[2].Cells[8].Value = "";
            dataGridView7.Rows[3].Cells[8].Value = "";

            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView7.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView7.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView7.Rows[0].Cells[2].Value);
                }
                if (dataGridView7.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView7.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView7.Rows[0].Cells[3].Value);
                }
                if (dataGridView7.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView7.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView7.Rows[0].Cells[4].Value);
                }
                if (dataGridView7.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView7.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView7.Rows[0].Cells[5].Value);
                }
                if (dataGridView7.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView7.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView7.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView7.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView7.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView7.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView7.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView7.Rows[1].Cells[2].Value);
                }
                if (dataGridView7.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView7.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView7.Rows[1].Cells[3].Value);
                }
                if (dataGridView7.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView7.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView7.Rows[1].Cells[4].Value);
                }
                if (dataGridView7.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView7.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView7.Rows[1].Cells[5].Value);
                }
                if (dataGridView7.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView7.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView7.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView7.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView7.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView7.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView7.Rows[2].Cells[2].Value);
                }
                if (dataGridView7.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView7.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView7.Rows[2].Cells[3].Value);
                }
                if (dataGridView7.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView7.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView7.Rows[2].Cells[4].Value);
                }
                if (dataGridView7.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView7.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView7.Rows[2].Cells[5].Value);
                }
                if (dataGridView7.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView7.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView7.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView7.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView7.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView7.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView7.Rows[3].Cells[2].Value);
                }
                if (dataGridView7.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView7.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView7.Rows[3].Cells[3].Value);
                }
                if (dataGridView7.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView7.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView7.Rows[3].Cells[4].Value);
                }
                if (dataGridView7.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView7.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView7.Rows[3].Cells[5].Value);
                }
                if (dataGridView7.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView7.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView7.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView7.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView7.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView7.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView7.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView7.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView7.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView7.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView7.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView7.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView7.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView7.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView7.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView7.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView7.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView7.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView7.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView7.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView7.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView7.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView7.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView7.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView7.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView7.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView7.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView7.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally 
            {
            
            }
        }

        private void Collation_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {

                Application.Exit();
            }
            catch
            {


            }
            finally
            { 
            
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {

                Form1 f1 = new Form1();
                f1.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {

                StreamWriter sw = new StreamWriter("FinalH.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Vice President Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView8.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView8.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView8.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[7].Value + "</td><th>" + dataGridView8.Rows[1].Cells[8].Value + "</td><th>" + dataGridView8.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView8.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView8.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView8.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView8.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView8.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalH.html");
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            dataGridView8.Rows[0].Cells[9].Value = "";
            dataGridView8.Rows[1].Cells[9].Value = "";
            dataGridView8.Rows[2].Cells[9].Value = "";
            dataGridView8.Rows[3].Cells[9].Value = "";
            dataGridView8.Rows[0].Cells[8].Value = "";
            dataGridView8.Rows[1].Cells[8].Value = "";
            dataGridView8.Rows[2].Cells[8].Value = "";
            dataGridView8.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView8.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView8.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView8.Rows[0].Cells[2].Value);
                }
                if (dataGridView8.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView8.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView8.Rows[0].Cells[3].Value);
                }
                if (dataGridView8.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView8.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView8.Rows[0].Cells[4].Value);
                }
                if (dataGridView8.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView8.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView8.Rows[0].Cells[5].Value);
                }
                if (dataGridView8.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView8.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView8.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView8.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView8.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView8.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView8.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView8.Rows[1].Cells[2].Value);
                }
                if (dataGridView8.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView8.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView8.Rows[1].Cells[3].Value);
                }
                if (dataGridView8.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView8.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView8.Rows[1].Cells[4].Value);
                }
                if (dataGridView8.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView8.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView8.Rows[1].Cells[5].Value);
                }
                if (dataGridView8.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView8.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView8.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView8.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView8.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView8.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView8.Rows[2].Cells[2].Value);
                }
                if (dataGridView8.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView8.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView8.Rows[2].Cells[3].Value);
                }
                if (dataGridView8.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView8.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView8.Rows[2].Cells[4].Value);
                }
                if (dataGridView8.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView8.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView8.Rows[2].Cells[5].Value);
                }
                if (dataGridView8.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView8.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView8.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView8.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView8.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView8.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView8.Rows[3].Cells[2].Value);
                }
                if (dataGridView8.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView8.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView8.Rows[3].Cells[3].Value);
                }
                if (dataGridView8.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView8.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView8.Rows[3].Cells[4].Value);
                }
                if (dataGridView8.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView8.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView8.Rows[3].Cells[5].Value);
                }
                if (dataGridView8.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView8.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView8.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView8.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView8.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView8.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView8.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView8.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView8.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView8.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView8.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView8.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView8.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView8.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView8.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView8.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView8.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView8.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView8.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView8.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView8.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView8.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView8.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView8.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView8.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView8.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView8.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView8.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { 
            }
            finally 
            { 
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {

                StreamWriter sw = new StreamWriter("FinalI.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Treasurer Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView9.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView9.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView9.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[7].Value + "</td><th>" + dataGridView9.Rows[1].Cells[8].Value + "</td><th>" + dataGridView9.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView9.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView9.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView9.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView9.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView9.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalI.html");
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            dataGridView9.Rows[0].Cells[9].Value = "";
            dataGridView9.Rows[1].Cells[9].Value = "";
            dataGridView9.Rows[2].Cells[9].Value = "";
            dataGridView9.Rows[3].Cells[9].Value = "";
            dataGridView9.Rows[0].Cells[8].Value = "";
            dataGridView9.Rows[1].Cells[8].Value = "";
            dataGridView9.Rows[2].Cells[8].Value = "";
            dataGridView9.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView9.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView9.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView9.Rows[0].Cells[2].Value);
                }
                if (dataGridView9.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView9.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView9.Rows[0].Cells[3].Value);
                }
                if (dataGridView9.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView9.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView9.Rows[0].Cells[4].Value);
                }
                if (dataGridView9.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView9.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView9.Rows[0].Cells[5].Value);
                }
                if (dataGridView9.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView9.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView9.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView9.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView9.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView9.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView9.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView9.Rows[1].Cells[2].Value);
                }
                if (dataGridView9.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView9.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView9.Rows[1].Cells[3].Value);
                }
                if (dataGridView9.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView9.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView9.Rows[1].Cells[4].Value);
                }
                if (dataGridView9.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView9.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView9.Rows[1].Cells[5].Value);
                }
                if (dataGridView9.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView9.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView9.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView9.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView9.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView9.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView9.Rows[2].Cells[2].Value);
                }
                if (dataGridView9.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView9.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView9.Rows[2].Cells[3].Value);
                }
                if (dataGridView9.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView9.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView9.Rows[2].Cells[4].Value);
                }
                if (dataGridView9.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView9.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView9.Rows[2].Cells[5].Value);
                }
                if (dataGridView9.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView9.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView9.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView9.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView9.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView9.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView9.Rows[3].Cells[2].Value);
                }
                if (dataGridView9.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView9.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView9.Rows[3].Cells[3].Value);
                }
                if (dataGridView9.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView9.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView9.Rows[3].Cells[4].Value);
                }
                if (dataGridView9.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView9.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView9.Rows[3].Cells[5].Value);
                }
                if (dataGridView9.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView9.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView9.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView9.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView9.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView9.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView9.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView9.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView9.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView9.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView9.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView9.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView9.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView9.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView9.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView9.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView9.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView9.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView9.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView9.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView9.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView9.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView9.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView9.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView9.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView9.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView9.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView9.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            try
            {

                StreamWriter sw = new StreamWriter("FinalJ.html");
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Librarian Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>ID</td><th>Names</td><th>PollA</td><th>PollB</td><th>PollC</td><th>PollD</td><th>PollE</td><th>TotalVotes</td><th>Percentage</td><th>Position</td><th>Agent Signature</td></tr><tr><th>" + dataGridView10.Rows[0].Cells[0].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[1].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[2].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[3].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[4].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[5].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[6].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[7].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[8].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[9].Value.ToString() + "</td><th>" + dataGridView10.Rows[0].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView10.Rows[1].Cells[0].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[1].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[2].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[3].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[4].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[5].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[6].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[7].Value + "</td><th>" + dataGridView10.Rows[1].Cells[8].Value + "</td><th>" + dataGridView10.Rows[1].Cells[9].Value.ToString() + "</td><th>" + dataGridView10.Rows[1].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView10.Rows[2].Cells[0].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[1].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[2].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[3].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[4].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[5].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[6].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[7].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[8].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[9].Value.ToString() + "</td><th>" + dataGridView10.Rows[2].Cells[10].Value.ToString() + "</td></tr><tr><th>" + dataGridView10.Rows[3].Cells[0].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[1].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[2].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[3].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[4].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[5].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[6].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[7].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[8].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[9].Value.ToString() + "</td><th>" + dataGridView10.Rows[3].Cells[10].Value.ToString() + "</td></tr></table>");
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                sw.Close();
                System.Diagnostics.Process.Start("FinalJ.html");
            }
            catch (Exception ex)
            { }
            finally
            {
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            dataGridView10.Rows[0].Cells[9].Value = "";
            dataGridView10.Rows[1].Cells[9].Value = "";
            dataGridView10.Rows[2].Cells[9].Value = "";
            dataGridView10.Rows[3].Cells[9].Value = "";
            dataGridView10.Rows[0].Cells[8].Value = "";
            dataGridView10.Rows[1].Cells[8].Value = "";
            dataGridView10.Rows[2].Cells[8].Value = "";
            dataGridView10.Rows[3].Cells[8].Value = "";
            try
            {
                int a = 0, b = 0, c = 0, d = 0, f = 0, g = 0;
                if (dataGridView10.Rows[0].Cells[2].Value.ToString() == "")
                { dataGridView10.Rows[0].Cells[2].Value = 0; }
                else
                {
                    a = Convert.ToInt32(dataGridView10.Rows[0].Cells[2].Value);
                }
                if (dataGridView10.Rows[0].Cells[3].Value.ToString() == "")
                { dataGridView10.Rows[0].Cells[3].Value = 0; }
                else
                {
                    b = Convert.ToInt32(dataGridView10.Rows[0].Cells[3].Value);
                }
                if (dataGridView10.Rows[0].Cells[4].Value.ToString() == "")
                { dataGridView10.Rows[0].Cells[4].Value = 0; }
                else
                {
                    c = Convert.ToInt32(dataGridView10.Rows[0].Cells[4].Value);
                }
                if (dataGridView10.Rows[0].Cells[5].Value.ToString() == "")
                { dataGridView10.Rows[0].Cells[5].Value = 0; }
                else
                {
                    d = Convert.ToInt32(dataGridView10.Rows[0].Cells[5].Value);
                }
                if (dataGridView10.Rows[0].Cells[6].Value.ToString() == "")
                { dataGridView10.Rows[0].Cells[6].Value = 0; }
                else
                {
                    f = Convert.ToInt32(dataGridView10.Rows[0].Cells[6].Value);
                }
                f = Convert.ToInt32(dataGridView10.Rows[0].Cells[6].Value);
                g = a + b + c + d + f;
                dataGridView10.Rows[0].Cells[7].Value = g;
                int h = 0, i = 0, j = 0, k = 0, l = 0, m = 0;
                if (dataGridView10.Rows[1].Cells[2].Value.ToString() == "")
                { dataGridView10.Rows[1].Cells[2].Value = 0; }
                else
                {
                    h = Convert.ToInt32(dataGridView10.Rows[1].Cells[2].Value);
                }
                if (dataGridView10.Rows[1].Cells[3].Value.ToString() == "")
                { dataGridView10.Rows[1].Cells[3].Value = 0; }
                else
                {
                    i = Convert.ToInt32(dataGridView10.Rows[1].Cells[3].Value);
                }
                if (dataGridView10.Rows[1].Cells[4].Value.ToString() == "")
                { dataGridView10.Rows[1].Cells[4].Value = 0; }
                else
                {
                    j = Convert.ToInt32(dataGridView10.Rows[1].Cells[4].Value);
                }
                if (dataGridView10.Rows[1].Cells[5].Value.ToString() == "")
                { dataGridView10.Rows[1].Cells[5].Value = 0; }
                else
                {
                    k = Convert.ToInt32(dataGridView10.Rows[1].Cells[5].Value);
                }
                if (dataGridView10.Rows[1].Cells[6].Value.ToString() == "")
                { dataGridView10.Rows[1].Cells[6].Value = 0; }
                else
                {
                    l = Convert.ToInt32(dataGridView10.Rows[1].Cells[6].Value);
                }
                m = h + i + j + k + l;
                dataGridView10.Rows[1].Cells[7].Value = m;

                //string q = o.ToString();
                //string r = p.ToString();
                int s = 0, t = 0, u = 0, v = 0, w = 0, x = 0;
                if (dataGridView10.Rows[2].Cells[2].Value.ToString() == "")
                { dataGridView10.Rows[2].Cells[2].Value = 0; }
                else
                {
                    s = Convert.ToInt32(dataGridView10.Rows[2].Cells[2].Value);
                }
                if (dataGridView10.Rows[2].Cells[3].Value.ToString() == "")
                { dataGridView10.Rows[2].Cells[3].Value = 0; }
                else
                {
                    t = Convert.ToInt32(dataGridView10.Rows[2].Cells[3].Value);
                }
                if (dataGridView10.Rows[2].Cells[4].Value.ToString() == "")
                { dataGridView10.Rows[2].Cells[4].Value = 0; }
                else
                {
                    u = Convert.ToInt32(dataGridView10.Rows[2].Cells[4].Value);
                }
                if (dataGridView10.Rows[2].Cells[5].Value.ToString() == "")
                { dataGridView10.Rows[2].Cells[5].Value = 0; }
                else
                {
                    v = Convert.ToInt32(dataGridView10.Rows[2].Cells[5].Value);
                }
                if (dataGridView10.Rows[2].Cells[6].Value.ToString() == "")
                { dataGridView10.Rows[2].Cells[6].Value = 0; }
                else
                {
                    w = Convert.ToInt32(dataGridView10.Rows[2].Cells[6].Value);
                }
                x = s + t + u + v + w;
                dataGridView10.Rows[2].Cells[7].Value = x;
                int za = 0, zb = 0, zc = 0, zd = 0, ze = 0, z = 0;
                if (dataGridView10.Rows[3].Cells[2].Value.ToString() == "")
                { dataGridView10.Rows[3].Cells[2].Value = 0; }
                else
                {
                    za = Convert.ToInt32(dataGridView10.Rows[3].Cells[2].Value);
                }
                if (dataGridView10.Rows[3].Cells[3].Value.ToString() == "")
                { dataGridView10.Rows[3].Cells[3].Value = 0; }
                else
                {
                    zb = Convert.ToInt32(dataGridView10.Rows[3].Cells[3].Value);
                }
                if (dataGridView10.Rows[3].Cells[4].Value.ToString() == "")
                { dataGridView10.Rows[3].Cells[4].Value = 0; }
                else
                {
                    zc = Convert.ToInt32(dataGridView10.Rows[3].Cells[4].Value);
                }
                if (dataGridView10.Rows[3].Cells[5].Value.ToString() == "")
                { dataGridView10.Rows[3].Cells[5].Value = 0; }
                else
                {
                    zd = Convert.ToInt32(dataGridView10.Rows[3].Cells[5].Value);
                }
                if (dataGridView10.Rows[3].Cells[6].Value.ToString() == "")
                { dataGridView10.Rows[3].Cells[6].Value = 0; }
                else
                {
                    ze = Convert.ToInt32(dataGridView10.Rows[3].Cells[6].Value);
                }
                z = za + zb + zc + zd + ze;
                dataGridView10.Rows[3].Cells[7].Value = z;
                int n, o, p, y, zf;

                if (dataGridView10.Rows[0].Cells[7].Value != null)
                //{ dataGridView1.Rows[0].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //o = Convert.ToInt32(dataGridView1.Rows[0].Cells[8].Value);
                    o = (g * 100) / n;
                    dataGridView10.Rows[0].Cells[8].Value = o.ToString();
                }

                if (dataGridView10.Rows[1].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    p = (m * 100) / n;
                    dataGridView10.Rows[1].Cells[8].Value = p.ToString();
                }
                if (dataGridView10.Rows[2].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    y = (x * 100) / n;
                    dataGridView10.Rows[2].Cells[8].Value = y.ToString();
                }
                if (dataGridView10.Rows[3].Cells[7].Value.ToString() != null)
                //{ dataGridView1.Rows[1].Cells[8].Value = 0; }
                //else
                {
                    n = g + m + x + z;
                    //p = Convert.ToInt32(dataGridView1.Rows[1].Cells[8].Value);
                    zf = (z * 100) / n;
                    dataGridView10.Rows[3].Cells[8].Value = zf.ToString();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
            }
            try
            {
                if (dataGridView10.Rows[0].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView10.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView10.Rows[0].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView10.Rows[0].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView10.Rows[1].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView10.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView10.Rows[1].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView10.Rows[1].Cells[9].Value = "Winner";
                    }
                }
                if (dataGridView10.Rows[2].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView10.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView10.Rows[2].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView10.Rows[2].Cells[9].Value = "Winner";
                    }

                }
                if (dataGridView10.Rows[3].Cells[7].Value.ToString() != "")
                {
                    int[] coleng = (from DataGridViewRow row in dataGridView10.Rows
                                    where row.Cells[8].FormattedValue.ToString() != string.Empty
                                    select Convert.ToInt32(row.Cells[8].FormattedValue)).ToArray();
                    if (dataGridView10.Rows[3].Cells[8].Value.ToString() == coleng.Max().ToString())
                    {
                        dataGridView10.Rows[3].Cells[9].Value = "Winner";
                    }

                }
            }
            catch (Exception ex)
            { }
            finally
            { 
            }
        }
    }
}
