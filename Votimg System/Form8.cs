﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.OleDb;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form8 : Form
    {
        int i, j, k, l, h;
        public OleDbConnection odc = new OleDbConnection();
        public Form8(string firstcome)
        {
            InitializeComponent();
            try
            {
                odc.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\Voting System.accdb;
Persist Security Info=False;";
                label6.Text = WindowsFormsApplication1.Properties.Settings.Default.Twoone.ToString();
                label7.Text = WindowsFormsApplication1.Properties.Settings.Default.Twotwo.ToString();
                label8.Text = WindowsFormsApplication1.Properties.Settings.Default.Twothree.ToString();
                label9.Text = WindowsFormsApplication1.Properties.Settings.Default.Twofour.ToString();
                label13.Text = firstcome;
            }
            catch (Exception ex)
            { }
            finally 
            { }
            System.Windows.Forms.Application.DoEvents();
        }
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                var bm = new Bitmap(pictureBox1.Image);
                var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
                var g = Graphics.FromImage(bm);
                g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
                pictureBox1.Image = bm;
                DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    a++;
                    int wa = a + WindowsFormsApplication1.Properties.Settings.Default.Twoone;
                    label6.Text = wa.ToString();
                    WindowsFormsApplication1.Properties.Settings.Default.Twoone = Convert.ToInt32(label6.Text);
                    WindowsFormsApplication1.Properties.Settings.Default.Save();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    Form9 f9 = new Form9(label13.Text);
                    f9.Show();
                    this.Hide();
                }
                else if (dia == DialogResult.No)
                {

                    pictureBox1.ImageLocation = @"C:\Users\Public\Documents\Voting Images\U.jpg";
                }
            }
            catch (Exception ex)
            {


            }
            finally
            { }
            StreamWriter sw = new StreamWriter("F.html");
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
               // j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
                
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Welfare Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            try
            {
                var bm = new Bitmap(pictureBox2.Image);
                var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
                var ga = Graphics.FromImage(bm);
                ga.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
                pictureBox2.Image = bm;
                DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    b++;
                    int wb = b + WindowsFormsApplication1.Properties.Settings.Default.Twotwo;
                    label7.Text = wb.ToString();
                    WindowsFormsApplication1.Properties.Settings.Default.Twotwo = Convert.ToInt32(label7.Text);
                    WindowsFormsApplication1.Properties.Settings.Default.Save();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    Form9 f9 = new Form9(label13.Text);
                    f9.Show();
                    this.Hide();
                }
                else if (dia == DialogResult.No)
                {

                    pictureBox2.ImageLocation = @"C:\Users\Public\Documents\Voting Images\V.jpg";
                }
            }
            catch (Exception ex)
            { 
            }
            finally
            {
            }
            StreamWriter sw = new StreamWriter("F.html");
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                // j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
               
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Welfare Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
                
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            /*try
            {

                var bm = new Bitmap(pictureBox3.Image);
                var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
                var g = Graphics.FromImage(bm);
                g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
                pictureBox3.Image = bm;
                DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
                if (dia == DialogResult.Yes)
                {
                    c++;
                    int wc = c + WindowsFormsApplication1.Properties.Settings.Default.Twothree;
                    label8.Text = wc.ToString();
                    WindowsFormsApplication1.Properties.Settings.Default.Twothree = Convert.ToInt32(label8.Text);
                    WindowsFormsApplication1.Properties.Settings.Default.Save();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    Form9 f9 = new Form9(label13.Text);
                    f9.Show();
                    this.Hide();
                }
                else if (dia == DialogResult.No)
                {

                    pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\W.jpg";
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            StreamWriter sw = new StreamWriter("F.html");
            try
            {
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text) + Convert.ToInt32(label8.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;

                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Welfare Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr><tr><th>{6}</td><th>{7}</td><th>{8}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString(), label4.Text, label8.Text, j.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");

            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();*/
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            /*var bm = new Bitmap(pictureBox4.Image);
            var fo = new System.Drawing.Font("Wingdings", 100, FontStyle.Bold, GraphicsUnit.Pixel);
            var g = Graphics.FromImage(bm);
            g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(10, 10));
            //g.DrawString("", fo, Brushes.Red, new System.Drawing.Point(100, 100));
            pictureBox4.Image = bm;
            DialogResult dia = MessageBox.Show("Are you sure you want to vote for this candidate?", "Confirmation", MessageBoxButtons.YesNo);
            if (dia == DialogResult.Yes)
            {
                d++;
                int wd = d + WindowsFormsApplication1.Properties.Settings.Default.Twofour;
                label9.Text = wd.ToString();
                WindowsFormsApplication1.Properties.Settings.Default.Twofour = Convert.ToInt32(label9.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;
                Form9 f9 = new Form9(label13.Text);
                f9.Show();
                this.Hide();
            }
            else if (dia == DialogResult.No)
            {

                pictureBox4.ImageLocation = @"C:\Users\Public\Documents\Voting Images\X.jpg";
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("F.html");
            try
            {
            DialogResult result = MessageBox.Show("Are you sure you want to go to the next page?", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Form9 f9 = new Form9(label13.Text);
                f9.Show();
                this.Hide();
            }

            l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                //j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
                
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Director of Welfare Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</html>");
               
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void Form8_Load(object sender, EventArgs e)
        {

            try
            {
                timer1.Start();
               pictureBox3.Visible = false;
                pictureBox4.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button5.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                label11.Visible = false;
                label12.Visible = false;
                textBox1.Visible = false;
                textBox2.Visible = false;
                button4.Visible = false;
                pictureBox1.ImageLocation = @"C:\Users\Public\Documents\Voting Images\U.jpg";
                pictureBox2.ImageLocation = @"C:\Users\Public\Documents\Voting Images\V.jpg";
               // pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\W.jpg";
            }
            catch (Exception ex)
            {
            }
            finally { 
            
            }
            //pictureBox3.ImageLocation = @"C:\Users\Public\Documents\Voting Images\W.jpg";
            //pictureBox4.ImageLocation = @"C:\Users\Public\Documents\Voting Images\X.jpg";
            try
            {
                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                string query = "Select Name from Title where ID=6 ";
                oc.CommandText = query;
                OleDbDataReader reader = oc.ExecuteReader();

                while (reader.Read())
                {
                    label1.Text = reader["Name"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
                OleDbCommand oc1 = new OleDbCommand();
                oc1.Connection = odc;
                string query1 = "Select BottomMessage from Title where ID=6 ";
                oc1.CommandText = query1;
                OleDbDataReader reader1 = oc1.ExecuteReader();

                while (reader1.Read())
                {
                    label10.Text = reader1["BottomMessage"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
                
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);


            }
            finally 
            {

                odc.Close();
            }
            try
            {
                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                string query = "Select Name from Welfare where ID=1 ";
                oc.CommandText = query;
                OleDbDataReader reader = oc.ExecuteReader();

                while (reader.Read())
                {
                    label2.Text = reader["Name"].ToString();
                    //label3.Text = reader["Name"].ToString();
                }
                //MessageBox.Show("You are connected");
                OleDbCommand occ = new OleDbCommand();
                occ.Connection = odc;
                string kuery = "Select Name from Welfare where ID=2 ";
                occ.CommandText = kuery;
                OleDbDataReader read = occ.ExecuteReader();
                while (read.Read())
                {
                    label3.Text = read["Name"].ToString();

                }
                /*OleDbCommand oca = new OleDbCommand();
               oca.Connection = odc;
               string uery = "Select Name from ASSecretary where ID=3 ";
               oca.CommandText = uery;
               OleDbDataReader readu = oca.ExecuteReader();
               while (readu.Read())
               {
                   label4.Text = readu["Name"].ToString();

               }

              OleDbCommand ocb = new OleDbCommand();
               ocb.Connection = odc;
               string kuer = "Select Name from Welfare where ID=4 ";
               ocb.CommandText = kuer;
               OleDbDataReader readb = ocb.ExecuteReader();
               while (readb.Read())
               {
                   label5.Text = readb["Name"].ToString();

               }*/

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally 
            {
                odc.Close();
            
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                int re1, re2, re3, re4;
                re1 = 0;
                re2 = 0;
                re3 = 0;
                re4 = 0;
                label6.Text = re1.ToString();
                label7.Text = re2.ToString();
                 //label8.Text = re3.ToString();
                //label9.Text = re4.ToString();
                WindowsFormsApplication1.Properties.Settings.Default.Twoone = Convert.ToInt32(label6.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
                WindowsFormsApplication1.Properties.Settings.Default.Twotwo = Convert.ToInt32(label7.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();
               /* WindowsFormsApplication1.Properties.Settings.Default.Twothree = Convert.ToInt32(label8.Text);
                WindowsFormsApplication1.Properties.Settings.Default.Save();*/
            }
            catch (Exception ex)
            {
            }
            finally
            { 
            
            }
            System.Windows.Forms.Application.DoEvents();
           /* WindowsFormsApplication1.Properties.Settings.Default.Twothree = Convert.ToInt32(label8.Text);
            WindowsFormsApplication1.Properties.Settings.Default.Save();
            WindowsFormsApplication1.Properties.Settings.Default.Twofour = Convert.ToInt32(label9.Text);
            WindowsFormsApplication1.Properties.Settings.Default.Save();*/
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("F.html");
            try
            {
                Microsoft.Office.Interop.Excel._Application exc = new Microsoft.Office.Interop.Excel.Application();
                Workbook wb = exc.Workbooks.Add(XlSheetType.xlWorksheet);
                Worksheet ws = exc.ActiveSheet;
                exc.Visible = true;
                //ws.Cells[1, 1] = "Js1";
                l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
                i = Convert.ToInt32(label6.Text) * 100 / l;
                k = Convert.ToInt32(label7.Text) * 100 / l;
                //j = Convert.ToInt32(label8.Text) * 100 / l;
                //int h = Convert.ToInt32(label9.Text) * 100 / l;
                ws.Cells[1, 1] = "Name";
                ws.Cells[1, 2] = "Votes";
                ws.Cells[1, 3] = "Percentage";
                //ws.Cells[1, 4] = "Number of votes";
                //ws.Cells[1, 5] = "Gender";

                ws.Cells[2, 1] = label2.Text;
                ws.Cells[2, 2] = label6.Text;
                ws.Cells[2, 3] = i.ToString() + "%";

                ws.Cells[3, 1] = label3.Text;
                ws.Cells[3, 2] = label7.Text;
                ws.Cells[3, 3] = k.ToString() + "%";

                /* ws.Cells[4, 1] = label4.Text;
                 ws.Cells[4, 2] = label8.Text;
                 ws.Cells[4, 3] = j.ToString() + "%";

                 ws.Cells[5, 1] = label5.Text;
                 ws.Cells[5, 2] = label9.Text;
                 ws.Cells[5, 3] = h.ToString() + "%";*/
               
                sw.WriteLine("<html>");
                sw.WriteLine("<head>");
                sw.WriteLine("<title>{0}</title>", label1.Text);
                sw.WriteLine("</head>");
                sw.WriteLine("<body bgcolor='grey'>");
                sw.WriteLine("<h1 style=color:black;text-align:center;line-height:4px>Assistant Secretary General Election Result</h1>");
                sw.WriteLine("<hr>");
                sw.WriteLine("<table border=5px; cellspacing=5px; align='center'; width=380px><tr color='black';font-size=20px><th>Names</td><th>Votes</td><th>Percentages</td></tr><tr><th>{0}</td><th>{1}</td><th>{2}</td></tr><tr><th>{3}</td><th>{4}</td><th>{5}<td></tr></table>", label2.Text, label6.Text, i.ToString(), label3.Text, label7.Text, k.ToString());
                sw.WriteLine("</body>");
                sw.WriteLine("</html>");
               
                System.Diagnostics.Process.Start("F.html");
            }
            catch (Exception ex)
            { }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void Form8_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                DialogResult di = MessageBox.Show("Are you sure you want to stop voting?", "Confirmation", MessageBoxButtons.YesNo);
                if (di == DialogResult.Yes)
                {
                    return;
                    System.Windows.Forms.Application.Exit(); }
                else if (di == DialogResult.No)

                { e.Cancel=true; }
            }
            catch (Exception ex)
            { }
            finally
            {


            }
            System.Windows.Forms.Application.DoEvents();
        }

        private void Form8_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            try
            {
                label11.Visible = true;
                label12.Visible = true;
                textBox1.Visible = true;
                textBox2.Visible = true;
                button4.Visible = true;
            }
            catch (Exception ex)
            { }
            finally
            { }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
              l = Convert.ToInt32(label6.Text) + Convert.ToInt32(label7.Text);
             i = Convert.ToInt32(label6.Text) * 100 / l;
             k = Convert.ToInt32(label7.Text) * 100 / l;
           // j = Convert.ToInt32(label8.Text) * 100 / l;
            //int h = Convert.ToInt32(label9.Text) * 100 / l;
            
                odc.Open();
                OleDbCommand oc = new OleDbCommand();
                oc.Connection = odc;
                oc.CommandText = "INSERT INTO [ResultASSecretary] ([Names],[Votes],[Percentages])  VALUES('" + label2.Text + "','" + label6.Text + "','" + i.ToString() + "')";
                oc.ExecuteNonQuery();


                OleDbCommand occ = new OleDbCommand();
                occ.Connection = odc;
                occ.CommandText = "INSERT INTO [ResultASSecretary] ([Names],[Votes],[Percentages])  VALUES('" + label3.Text + "','" + label7.Text + "','" + k.ToString() + "')";
                occ.ExecuteNonQuery();

                /*
                                OleDbCommand ocd = new OleDbCommand();
                                ocd.Connection = odc;
                                ocd.CommandText = "INSERT INTO [ResultASSecretary] ([Names],[Votes],[Percentages])  VALUES('" + label4.Text + "','" + label8.Text + "','" + j.ToString() + "')";
                                ocd.ExecuteNonQuery();

                
                                OleDbCommand oce = new OleDbCommand();
                                oce.Connection = odc;
                                oce.CommandText = "INSERT INTO [Result for Welfare] ([Names],[Votes],[Percentages])  VALUES('" + label5.Text + "','" + label9.Text + "','" + h.ToString() + "')";
                                oce.ExecuteNonQuery();
                                */
               
           
            
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                odc.Close();
            }
            try
            {
                if (textBox1.Text == "awosusi" && textBox2.Text == "oluwatopebest")
                {
                    button1.Visible = true;
                    button2.Visible = true;
                    button3.Visible = true;
                    button5.Visible = true;
                    label6.Visible = true;
                    label7.Visible = true;
                   // label8.Visible = true;
                    //label9.Visible = true;
                }
                else
                {
                    MessageBox.Show("Access denied!");
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            System.Windows.Forms.Application.DoEvents();
        }

        private void textBox1_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(textBox1, "Please insert your Username");
        }

        private void textBox2_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(textBox2, "Please insert your Password");
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button4, "Please click to check result on windows form");
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button3, "Please click to check result on Excel page");
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button1, "Please click to go to the next page");
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button2, "Click to reset the results.");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox1, " Click to vote for this candidate");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox2, " Click to vote for this candidate");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            //toolTip1.SetToolTip(pictureBox3, " Click to vote for this candidate");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            //toolTip1.SetToolTip(pictureBox4, " Click to vote for this candidate");
        }
        bool ani = true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                label14.Text = DateTime.Now.ToLongTimeString();
                if (ani)
                    label13.Location = new System.Drawing.Point(label13.Location.X + 5, label13.Location.Y);
                else
                    label13.Location = new System.Drawing.Point(label13.Location.X - 1044, label13.Location.Y);
                if (label13.Location.X + label13.Width >= this.Width + 180)
                    ani = false;
                if (label13.Location.X <= 0)
                    ani = true;
            }
            catch (Exception ex)
            { }
            finally 
            { }
            System.Windows.Forms.Application.DoEvents();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Form1 f1 = new Form1();
                f1.Show();
                this.Hide();
            }
            catch (Exception ex)
            { }
            finally 
            { }
        }
    }
}
